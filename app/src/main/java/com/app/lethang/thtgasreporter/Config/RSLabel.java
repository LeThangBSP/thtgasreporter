package com.app.lethang.thtgasreporter.Config;

import android.Manifest;

public class RSLabel {
    public static final int MY_APP_REQUEST_CODE = 17;
    public static final String[] appPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final String DETAIL_SHEET_NAME = "Bảng báo cáo chi tiết";
    public static final String SUMMARY_SHEET_NAME = "Bảng tổng hợp";
    public static final String BILL_SHEET_NAME = "Phiếu nhập xuất khí";

}
