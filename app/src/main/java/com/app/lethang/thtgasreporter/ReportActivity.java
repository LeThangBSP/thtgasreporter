package com.app.lethang.thtgasreporter;

import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.lethang.thtgasreporter.Adapter.SupplierSpinnerAdapter;
import com.app.lethang.thtgasreporter.Config.RSLabel;
import com.app.lethang.thtgasreporter.Helper.DBHelper;
import com.app.lethang.thtgasreporter.Helper.ExcelExporter;
import com.app.lethang.thtgasreporter.Helper.MailHelper;
import com.app.lethang.thtgasreporter.Helper.PermissionHelper;
import com.app.lethang.thtgasreporter.Helper.RecordHelper;
import com.app.lethang.thtgasreporter.Helper.SupplierHelper;
import com.app.lethang.thtgasreporter.Model.Record;
import com.app.lethang.thtgasreporter.Picker.DatePickerFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ReportActivity extends AppCompatActivity implements View.OnClickListener, DatePickerFragment.DatePickerCallback {
    public MailHelper mailHelper;
    public DBHelper dbHelper;
    ExcelExporter excelExporter;
    PermissionHelper permissionHelper;

    private EditText mFromDateEditText, mToDateEditText, mLastTriggerDatePicker, mEmailEditText;
    private Spinner mSupplierSpinner;
    private SupplierSpinnerAdapter mSupplierAdapter;
    private SupplierHelper mSupplierHelper;
    private RadioGroup mReportTypeRadioGroup;
    private Button mSendMailButton;

    private Context mSelfContext = this;
    private FragmentManager mFragmentManager;
    private DatePickerFragment mDatePicker;

    private long mFromDate, mToDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        try {
            mailHelper = new MailHelper(this);
            dbHelper = new DBHelper(this);
            excelExporter = new ExcelExporter(this, dbHelper);
            permissionHelper = new PermissionHelper(this);

            permissionHelper.checkPermissionRequestIfNeeded();

            mFragmentManager = getFragmentManager();
            //sendDebugMail();
            getHelpers();
            getUIComponents();
            setupDateTimePicker();

            //sendDebugMail();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void getHelpers() {
        mSupplierHelper = SupplierHelper.getInstance(null, false);
    }

    private void getUIComponents() {
        mFromDateEditText = findViewById(R.id.editText_FromDay);
        mToDateEditText = findViewById(R.id.editText_ToDay);
        mSupplierSpinner = findViewById(R.id.spinner_supplier);

        mFromDateEditText.setOnClickListener(this);
        mToDateEditText.setOnClickListener(this);

        mSupplierAdapter = new SupplierSpinnerAdapter(this, android.R.layout.simple_spinner_dropdown_item, mSupplierHelper);
        mSupplierSpinner.setAdapter(mSupplierAdapter);

        mReportTypeRadioGroup = findViewById(R.id.radioGroup_reportType);
        mEmailEditText = findViewById(R.id.editText_emails);

        mSendMailButton = findViewById(R.id.button_SendMail);

    }

    private void setupDateTimePicker() {
        mDatePicker = new DatePickerFragment();
        mDatePicker.setCallBackListener(this);
    }

    private ExcelExporter.SheetType getReportType() {
        switch (mReportTypeRadioGroup.getCheckedRadioButtonId()) {
            case R.id.radioButton_detail:
                return ExcelExporter.SheetType.Detail;
            case R.id.radioButton_summary:
                return ExcelExporter.SheetType.Summary;
            case R.id.radioButton_bill:
                return ExcelExporter.SheetType.Bill;
        }
        return ExcelExporter.SheetType.Detail;
    }

    private String[] getEmails() {
        return mEmailEditText.getText().toString().split(" ");
    }

    private void sendDebugMail() {

        ExcelExporter.SheetType reportType = getReportType();
        Toast.makeText(this, dbHelper.checkDatabase() ? "YES" : "NO", Toast.LENGTH_SHORT).show();

        ArrayList<Record> result = RecordHelper.getInstance(dbHelper, false).queryRecords(mFromDate, mToDate);

//        if (excelExporter.createFileExternalStorate(true)) {
//            excelExporter.writeFileContent(reportType);
//            String[] emails = {"ketoan3@thaihungthinh.com.vn,lethangmmt04@gmail.com"};
//            mailHelper.sendEmailTo(emails, excelExporter.createdFile);
//        } else if (excelExporter.createFileInternalStorage(true)) {
//            excelExporter.writeFileContent(reportType);
//            String[] emails = {"ketoan3@thaihungthinh.com.vn,lethangmmt04@gmail.com"};
//            mailHelper.sendEmailTo(emails, excelExporter.createdFile);
//        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RSLabel.MY_APP_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                for (int i = 0; i < grantResults.length; i++)
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, permissions[i] + " is deny", Toast.LENGTH_LONG).show();
                    }
        }
    }

    @Override
    public void OnDateSet(int year, int month, int dayOfMonth) {
        mLastTriggerDatePicker.setText(dayOfMonth + "/" + month + "/" + year);
        Date pickDate = new GregorianCalendar(year, month, dayOfMonth).getTime();
        if (mLastTriggerDatePicker == mFromDateEditText) {
            mFromDate = pickDate.getTime();
        }
        else
            mToDate = pickDate.getTime();
        Log.d("Date set", "From: " + mFromDate + " To: " + mToDate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editText_FromDay:
            case R.id.editText_ToDay:
                mLastTriggerDatePicker = (EditText) v;
                if (!mDatePicker.isAdded())
                    mDatePicker.show(mFragmentManager, "datePicker");
                break;
            case R.id.button_SendMail:
                sendDebugMail();
        }
    }


//    void requestPermission() {
//        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(permissions, 20);
//        }
//    }


}
