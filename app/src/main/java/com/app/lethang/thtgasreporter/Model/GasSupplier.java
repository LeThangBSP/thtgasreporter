package com.app.lethang.thtgasreporter.Model;

public class GasSupplier {
    public int id;
    public String name;

    public GasSupplier (int _id, String _name) {
        name = _name;
        id = _id;
    }
}
