package com.app.lethang.thtgasreporter.Picker;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public interface DatePickerCallback {
        void OnDateSet(int year, int month, int day);
    }

    private DatePickerCallback mCallBack;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (mCallBack != null)
            mCallBack.OnDateSet(year, month, dayOfMonth);
    }

    public void setCallBackListener(DatePickerCallback receiver) {
        mCallBack = receiver;
    }
}
