package com.app.lethang.thtgasreporter.Helper;

import android.database.Cursor;
import android.util.Log;

import com.app.lethang.thtgasreporter.Model.Gas;
import com.app.lethang.thtgasreporter.Model.GasSupplier;

import java.util.ArrayList;

public class SupplierHelper {
    public static final String TABLE_NAME = "Supplier";
    public static final String COL_NAME = "name";
    public static final String COL_ID = "id";

    private static final SupplierHelper ourInstance = new SupplierHelper();

    private DBHelper mDbHelper;

    private ArrayList<GasSupplier> mSuppliers = new ArrayList<GasSupplier>();
    public ArrayList<GasSupplier> getSuppliers() {
        return mSuppliers;
    }

    public static SupplierHelper getInstance(DBHelper helper, boolean forceUpdate) {
        if (helper != null && ourInstance.mDbHelper == null)
            ourInstance.mDbHelper = helper;
        if (ourInstance.mDbHelper == null) {
            Log.e("GasHelper", "DbHelper is null");
        }
        if (forceUpdate)
            ourInstance.update();
        return ourInstance;
    }

    public String getSupplierName(int supplierId) {
        for (GasSupplier supplier :
                mSuppliers) {
            if (supplier.id == supplierId)
                return supplier.name;
        }
        return "";
    }

    public void update() {
        Cursor c =  mDbHelper.getSuppliers();

        if (c.moveToFirst()) {
            mSuppliers.clear();
            while ( !c.isAfterLast() ) {
                String name = c.getString( c.getColumnIndex(COL_NAME));
                int id = c.getInt(c.getColumnIndex(COL_ID));
                mSuppliers.add(new GasSupplier(id, name));
                c.moveToNext();
            }
        }
    }

    private SupplierHelper() {

    }
}
