package com.app.lethang.thtgasreporter.Helper;

import android.database.Cursor;
import android.util.Log;

import com.app.lethang.thtgasreporter.Model.Gas;

import java.util.ArrayList;
import java.util.List;

public class GasHelper {
    public static final String TABLE_NAME = "GasType";
    public static final String COL_NAME = "name";
    public static final String COL_ID = "id";

    private static final GasHelper ourInstance = new GasHelper();

    private DBHelper mDbHelper;

    private ArrayList<Gas> mGasList = new ArrayList<Gas>();
    public ArrayList<Gas> getAllGas() {
        return mGasList;
    }

    public static GasHelper getInstance(DBHelper helper, boolean forceUpdate) {
        if (helper != null && ourInstance.mDbHelper == null)
            ourInstance.mDbHelper = helper;
        if (ourInstance.mDbHelper == null) {
            Log.e("GasHelper", "DbHelper is null");
        }
        if (forceUpdate)
            ourInstance.update();
        return ourInstance;
    }

    public void update() {
        Cursor c =  mDbHelper.getGasTypes();

        if (c.moveToFirst()) {
            mGasList.clear();
            while ( !c.isAfterLast() ) {
                String name = c.getString( c.getColumnIndex(COL_NAME));
                int id = c.getInt(c.getColumnIndex(COL_ID));
                mGasList.add(new Gas(id, name));
                c.moveToNext();
            }
        }
    }

    public String getGasName(int gasId) {
        for (Gas gas :
                mGasList) {
            if (gas.id == gasId)
                return gas.name;
        }
        return "";
    }

    private GasHelper() {

    }
}
