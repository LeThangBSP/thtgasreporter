package com.app.lethang.thtgasreporter.Model;

import android.database.Cursor;
import android.text.format.DateFormat;

import com.app.lethang.thtgasreporter.Helper.DBHelper;
import com.app.lethang.thtgasreporter.Helper.GasHelper;
import com.app.lethang.thtgasreporter.Helper.RecordHelper;
import com.app.lethang.thtgasreporter.Helper.SupplierHelper;

import java.util.Date;

public class Record {
    public String bottleCode;
    public int gasType;
    public int supplier;
    public long importDate;
    public long exportDate;
    public float importWeight;
    public float exportWeight;

    public Record () {
    }

    public Record(Cursor c) {
        gasType = c.getInt(c.getColumnIndex(RecordHelper.COL_GASTYPE));
        exportWeight = c.getFloat(c.getColumnIndex(RecordHelper.COL_EXPORT_WEIGHT));
        importWeight = c.getFloat(c.getColumnIndex(RecordHelper.COL_IMPORT_WEIGHT));
        importDate = c.getLong(c.getColumnIndex(RecordHelper.COL_IMPORT_DATE));
        exportDate = c.getLong(c.getColumnIndex(RecordHelper.COL_EXPORT_DATE));
        supplier = c.getInt(c.getColumnIndex(RecordHelper.COL_SUPPLIER));
        bottleCode = c.getString(c.getColumnIndex(RecordHelper.COL_BOTTLE_CODE));
    }

    public Record (String _bottleCode, int _gasType, int _supplier, long _importDate, long _exportDate, float _importWeight, float _exportWeight) {
        bottleCode = _bottleCode;
        gasType = _gasType;
        supplier = _supplier;
        importDate = _importDate;
        exportDate = _exportDate;
        importWeight = _importWeight;
        exportWeight = _exportWeight;
    }

    public String[] toRowOfDetailSheet(DBHelper dbHelper) {
        String[] cells = new String[9];
        cells[0] = bottleCode;
        cells[1] = GasHelper.getInstance(null, false).getGasName(gasType);
        cells[2] = SupplierHelper.getInstance(null, false).getSupplierName(supplier);
        cells[3] = getDateFormat(importDate);
        cells[4] = importWeight > 0 ? String.valueOf(importWeight) : "";
        cells[5] = getDateFormat(exportDate);
        cells[6] = exportWeight > 0 ? String.valueOf(exportWeight) : "";
        cells[7] = getUsedWeight();
        cells[8] = "Note";
        return cells;
    }

    @Override
    public String toString() {
        String seperator = "  ";
        String str = "bottleCode: " + bottleCode + seperator;
        str += "gasType: " + gasType + seperator;
        str += "supplier: " + supplier + seperator;
        str += "importDate: " + importDate + seperator;
        str += "importWeight: " + importWeight + seperator;
        str += "exportDate: " + exportDate + seperator;
        str += "exportWeight: " + exportWeight;
        return str;
    }

    private String getDateFormat(long time) {
        if (time == 0)
            return "";

        Date date = new Date(time);
        DateFormat df = new android.text.format.DateFormat();
        return df.format("dd-MM-yyyy hh:mm:ss a", date).toString();
    }

    private String getUsedWeight() {
        if (exportWeight == 0)
            return "";
        return String.valueOf(importWeight - exportWeight);
    }
}
