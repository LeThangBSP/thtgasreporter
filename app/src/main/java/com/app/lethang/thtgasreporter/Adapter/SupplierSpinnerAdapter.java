package com.app.lethang.thtgasreporter.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.lethang.thtgasreporter.Helper.GasHelper;
import com.app.lethang.thtgasreporter.Helper.SupplierHelper;
import com.app.lethang.thtgasreporter.Model.Gas;
import com.app.lethang.thtgasreporter.Model.GasSupplier;
import com.app.lethang.thtgasreporter.Model.Record;

import java.util.ArrayList;

public class SupplierSpinnerAdapter extends ArrayAdapter<Gas> {
    private Context mContext;
    private ArrayList<GasSupplier> listSupplier;
    private int mResourceId;

    public SupplierSpinnerAdapter(@NonNull Context context, int resource, SupplierHelper helper) {
        super(context, resource);
        listSupplier = helper.getSuppliers();
        mResourceId = resource;
        mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(mResourceId, null);
        TextView textView = view.findViewById(android.R.id.text1);
        textView.setText(listSupplier.get(position).name);
        if (position % 2 == 0)
            textView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        return view;
        //return createTextViewWithTextColor(position, android.R.color.black);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return getDropDownView(position, convertView, parent);
        return createTextViewWithTextColor(position, android.R.color.holo_red_dark);
    }

    private TextView createTextViewWithTextColor(int position, int colorId) {
        TextView textView = new TextView(mContext);
        textView.setText(listSupplier.get(position).name);
        textView.setTextColor(mContext.getResources().getColor(colorId));
        return textView;
    }

    @Override
    public int getCount() {
        return listSupplier.size();
    }

    public int getPosition(Record record) {
        for (GasSupplier supplier :
                listSupplier) {
            if (supplier.id == record.supplier)
                return listSupplier.indexOf(supplier);
        }
        return -1;
    }
}
