package com.app.lethang.thtgasreporter.Helper;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

import com.app.lethang.thtgasreporter.Config.RSLabel;
import com.app.lethang.thtgasreporter.Model.Record;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Locale;

import jxl.Cell;
import jxl.CellView;
import jxl.Range;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.format.Font;
import jxl.format.Format;
import jxl.format.Orientation;
import jxl.format.Pattern;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.read.biff.PasswordException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ExcelExporter {
    public enum SheetType {
        Detail(0, "Detail", RSLabel.DETAIL_SHEET_NAME), Summary(1, "Summary", RSLabel.SUMMARY_SHEET_NAME), Bill(2, "Bill", RSLabel.BILL_SHEET_NAME);
        private final int value;
        private final String templateSheetName;
        private final String outSheetName;

        SheetType(int value, String name, String outName) {
            this.value = value;
            this.templateSheetName = name;
            this.outSheetName = outName;
        }

        public int getValue() {
            return this.value;
        }

        public String getTemplateSheetName() {
            return this.templateSheetName;
        }

        public String getOutSheetName() {
            return this.outSheetName;
        }
    }

    public static final String TEMPLATE_NAME = "ReportTemplate.xls";

    private String _filePrefix = "GasReport";
    private String _fileExtension = ".xls";

    private Context _context;
    private DBHelper _dbHelper;
    private RecordHelper _recordHelper;
    private String _filePath = "";

    public File createdFile;

    public Workbook templateWorkbook;

    public ExcelExporter(Context context, DBHelper dbHelper) {
        _context = context;
        _dbHelper = dbHelper;
        _recordHelper = RecordHelper.getInstance(_dbHelper, false);
    }

    private void checkFilePermission() {
        boolean canRead = createdFile.canRead();
        boolean canWrite = createdFile.canWrite();
        Log.d("ExcelExporter", "canRead=" + canRead + ", canWrite=" + canWrite);
    }

    public boolean createFileExternalStorate(boolean copyTemplate) {
        boolean isAvailable = isExternalStorageWritable();

        if (!isAvailable)
            return false;

        String csvFile = _filePrefix + _fileExtension;
        File directory = getPublicStorageDir();
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {
            //file path
            createdFile = new File(directory.getAbsolutePath(), csvFile);
            boolean success = createdFile.exists();

            if (!success) {
                success = createdFile.createNewFile();
            }
            if (success) {
                createdFile.setReadable(true, false);
                checkFilePermission();
            }

            //writeTestContent();
            Log.d("ExcelExporter", "[External] File is created at " + createdFile.getPath());
            return success;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean createFileInternalStorage(boolean copyTemplate) {
        try {
            String csvFile = _filePrefix + _fileExtension;
            createdFile = new File(_context.getFilesDir(), csvFile);
            boolean success = createdFile.exists();
            if (!success)
                success = createdFile.createNewFile();
            if (success) {
                checkFilePermission();
            }
            _filePath = createdFile.getPath();
            //writeTestContent();
            Log.d("ExcelExporter", "[Internal] File is created at " + createdFile.getPath());
            return success;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void copyFromTemplateFile() {
        try {
            InputStream myInput = _context.getAssets().open(TEMPLATE_NAME);
            OutputStream myOutput = new FileOutputStream(createdFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private WritableWorkbook copyFromTemplatedWorkbook() {
        try {
            Workbook templateWb = Workbook.getWorkbook(_context.getAssets().open(TEMPLATE_NAME));
            return Workbook.createWorkbook(createdFile, templateWb);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loadTemplateWorkbook() {
        try {
            if (templateWorkbook != null)
                return;

            InputStream myInput = _context.getAssets().open(TEMPLATE_NAME);
            if (myInput != null) {
                templateWorkbook = Workbook.getWorkbook(myInput);
                if (templateWorkbook != null)
                    Log.d("ExcelExporter", "template workbook load successful");
            } else {
                Log.e("ExcelExporter", "not found template workbook file.");
            }
            myInput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeUnuseSheets(WritableWorkbook wb, SheetType keepSheet) {
        try {
            WritableSheet sheet;
            int i = 0;
            while (wb.getNumberOfSheets() > 1) {
                sheet = wb.getSheet(i);
                if (!sheet.getName().equals(keepSheet.getTemplateSheetName())) {
                    wb.removeSheet(i);
                } else {
                    sheet.setName(keepSheet.getOutSheetName());
                    i++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public File getPublicStorageDir() {
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        try {
            if (!file.mkdirs()) {
                Log.e("Excel Exporter", "Directory not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }


    public void writeFileContent(SheetType sheetType) {
        try {
            //loadTemplateWorkbook();
            //WritableWorkbook workbook = createWorkbookSetting();
            WritableWorkbook workbook = copyFromTemplatedWorkbook();
            removeUnuseSheets(workbook, sheetType);

            if (workbook != null) {
                //createSheet(workbook,sheetType);
                //importDBContent(workbook);
                generateSheetContent(workbook, 0, sheetType);
                workbook.write();
                workbook.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createSheet(WritableWorkbook workbook, SheetType sheetType) {
        Sheet sheet = templateWorkbook.getSheet(sheetType.getValue());
        if (sheet != null) {
            importSheet(workbook, sheet.getName(), 0, sheet);
        }
    }

    private WritableWorkbook createWorkbookSetting() {
        try {
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));

            return Workbook.createWorkbook(createdFile, wbSettings);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private void importDBContent(WritableWorkbook workbook) {
        String[] tableNames = _dbHelper.getTableNames();
        Cursor tableCursor;

        for (int i = 0; i < tableNames.length; i++) {
            String[] colNames = _dbHelper.getColumnsName(tableNames[i]);
            tableCursor = _dbHelper.getTable(tableNames[i]);

            WritableSheet sheet = createSheet(workbook, tableNames[i], i);
            createHeader(sheet, colNames);
            createContent(tableCursor, colNames, sheet);
        }
    }

    private void generateSheetContent(WritableWorkbook workbook, int sheetPos, SheetType sheetType) {
        WritableSheet sheet = workbook.getSheet(sheetPos);
        switch (sheetType) {
            case Detail:
                generateDetailSheet(sheet);
                break;
            case Summary:
                //Todo: implement
                break;
            case Bill:
                //Todo: implement
                break;
        }

    }

    private WritableSheet createSheet(WritableWorkbook workbook, String sheetName, int sheetPosition) {
        return workbook.createSheet(sheetName, sheetPosition);
    }

    private WritableSheet importSheet(WritableWorkbook workbook, String sheetName, int sheetPosition, Sheet targetSheet) {
        return workbook.importSheet(sheetName, sheetPosition, targetSheet);
    }

    private void createHeader(WritableSheet sheet, String[] headers) {
        try {
            for (int i = 0; i < headers.length; i++) {
                sheet.addCell(new Label(i, 0, headers[i]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createContent(Cursor tableCursor, String[] columnNames, WritableSheet sheet) {
        try {
            if (tableCursor.moveToFirst()) {
                do {
                    for (int i = 0; i < columnNames.length; i++) {
                        String content = tableCursor.getString(tableCursor.getColumnIndex(columnNames[i]));
                        int pos = tableCursor.getPosition() + 1;
                        sheet.addCell(new Label(i, pos, content));
                    }
                } while (tableCursor.moveToNext());
                tableCursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAutoSizeCell(WritableSheet sheet, int fromCol, int toCol) {
        for (int i = fromCol; i <= toCol; i++) {
            CellView cellView = sheet.getColumnView(i);
            cellView.setAutosize(true);
            sheet.setColumnView(i, cellView);
        }
    }

    private WritableCellFormat getCellFormatBold(CellFormat cellFormat) {
        try {
            WritableFont cellFont = new WritableFont(cellFormat.getFont());
            cellFont.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat wFormat = new WritableCellFormat(cellFormat);
            wFormat.setFont(cellFont);
            return wFormat;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new WritableCellFormat(cellFormat);
    }

    private WritableCellFormat setBorder(WritableCell cell, Border border, BorderLineStyle borderStyle) {
        try {
            WritableCellFormat wFormat = new WritableCellFormat(cell.getCellFormat());
            wFormat.setBorder(border, borderStyle);
            cell.setCellFormat(wFormat);
            return wFormat;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new WritableCellFormat(cell.getCellFormat());
    }

    private void generateDetailSheet(WritableSheet sheet) {
        int rowIndex = 2;
        String[] args;
        Label currentCell;
        ArrayList<Record> records = _recordHelper.getRecords();
        for (Record r : records) {
            args = r.toRowOfDetailSheet(null);
            for (int i = 0; i < args.length; i++)
                try {
                    currentCell = new Label(i, rowIndex, args[i]);
                    setBorder(currentCell, Border.ALL, BorderLineStyle.THIN);
                    sheet.addCell(currentCell);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            rowIndex++;
        }
        setAutoSizeCell(sheet, 1, sheet.getColumns());

        try {
            Label label = new Label(7, rowIndex + 2, "NGƯỜI LẬP");
            label.setCellFormat(getCellFormatBold(label.getCellFormat()));
            sheet.addCell(label);

            label = (Label) sheet.getWritableCell("A2");
            label.setString("MÃ CHAI");

            label = (Label) sheet.getWritableCell("I2");
            label.setString("GHI CHÚ");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void generateBillSheet(WritableSheet sheet) {

    }
}
