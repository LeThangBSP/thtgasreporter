package com.app.lethang.thtgasreporter.Model;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.ResultSet;

public class Gas {
    public int id;
    public String name;

    public Gas(int _id, String _name) {
        id = _id;
        name = _name;
    }

    public static Gas newInstance(ResultSet rs) {
        try {
            rs.next();

            // Object object = rs.getObject(1);

            byte[] buf = rs.getBytes(1);
            ObjectInputStream objectIn = null;
            if (buf != null)
                objectIn = new ObjectInputStream(new ByteArrayInputStream(buf));

            Object deSerializedObject = objectIn.readObject();
            rs.close();
            return (Gas) deSerializedObject;
        } catch (Exception e) {

        }
        return null;
    }
}
