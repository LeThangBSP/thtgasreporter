package com.app.lethang.thtgasreporter.Helper;

import android.database.Cursor;
import android.util.Log;

import com.app.lethang.thtgasreporter.Fragment.RecordFragment;
import com.app.lethang.thtgasreporter.Model.Record;

import java.util.ArrayList;

public class RecordHelper {
    public static final String TABLE_NAME = "Record";
    public static final String COL_BOTTLE_CODE = "ma_quet";
    public static final String COL_GASTYPE = "loai_khi";
    public static final String COL_SUPPLIER = "nha_cung_cap";
    public static final String COL_IMPORT_DATE = "ngay_truoc";
    public static final String COL_IMPORT_WEIGHT = "trong_luong_truoc";
    public static final String COL_EXPORT_DATE = "ngay_sau";
    public static final String COL_EXPORT_WEIGHT = "trong_luong_sau";

    public enum ResultCode {VALID, NOT_EXPORT_YET, NOT_IMPORT_YET, ZERO_WEIGHT, BOTTLE_CODE_EMPTY, INSERT_DB_FAIL, UPDATE_DB_FAIL}

    private static final RecordHelper ourInstance = new RecordHelper();

    private DBHelper mDbHelper;

    private ArrayList<Record> mRecords = new ArrayList<>();

    public ArrayList<Record> getRecords() {
        return mRecords;
    }

    private ArrayList<Record> mTempQueryRecords = new ArrayList<>();

    public ArrayList<Record> queryRecords(long fromDay, long toDay) {
        mTempQueryRecords.clear();
        try {
            Cursor c = mDbHelper.getRecords(fromDay, toDay);
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    Record record = new Record(c);
                    mTempQueryRecords.add(record);
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mTempQueryRecords;
    }

    public static RecordHelper getInstance(DBHelper helper, boolean forceUpdate) {
        if (helper != null && ourInstance.mDbHelper == null)
            ourInstance.mDbHelper = helper;
        if (ourInstance.mDbHelper == null) {
            Log.e("GasHelper", "DbHelper is null");
        }
        if (forceUpdate)
            ourInstance.update();
        return ourInstance;
    }

    public void update() {
        Cursor c =  mDbHelper.getRecords();

        if (c.moveToFirst()) {
            mRecords.clear();
            while ( !c.isAfterLast() ) {
                Record record = new Record(c);
//                record.gasType  = c.getInt( c.getColumnIndex(COL_GASTYPE));
//                record.exportWeight = c.getFloat(c.getColumnIndex(COL_EXPORT_WEIGHT));
//                record.importWeight = c.getFloat(c.getColumnIndex(COL_IMPORT_WEIGHT));
//                record.importDate = c.getLong(c.getColumnIndex(COL_IMPORT_DATE));
//                record.exportDate = c.getLong(c.getColumnIndex(COL_EXPORT_DATE));
//                record.supplier = c.getInt(c.getColumnIndex(COL_SUPPLIER));
//                record.bottleCode = c.getString(c.getColumnIndex(COL_BOTTLE_CODE));
                mRecords.add(record);
                c.moveToNext();
            }
        }
    }


    public Record getRecord(String bottleCode) {
        for (Record r :
                mRecords) {
            if (r.bottleCode.equals(bottleCode))
                return r;
        }
        return null;
    }

    public ResultCode exportGas(Record record) {
        ResultCode result = checkValid(record, RecordFragment.RecordType.EXPORT);

        if (result != ResultCode.VALID) {
            return result;
        }

        if (mDbHelper.updateRecord(record)) {
            Log.d(this.getClass().toString(), "update record successful.");
            update();
            //Todo: manually add record
        } else
            return ResultCode.UPDATE_DB_FAIL;

        return ResultCode.VALID;
    }

    public ResultCode importGas(Record record) {
        ResultCode result = checkValid(record, RecordFragment.RecordType.IMPORT);

        if (result != ResultCode.VALID) {
            return result;
        }

        if (mDbHelper.insertRecord(record)) {
            Log.d(this.getClass().toString(), "insert record successful.");
            update();
            //Todo: manually add record
        } else
            result = ResultCode.INSERT_DB_FAIL;

        return result;
    }

    boolean gcZeroWeight;
    boolean gcBottleCodeEmpty;
    boolean gcSameBottleCode;
    boolean gcExported;
    boolean gcImportMode;
    boolean gcValidExportBottleCode;


    private ResultCode checkValid(Record record, RecordFragment.RecordType type) {
        gcBottleCodeEmpty = record.bottleCode.length() == 0;
        if (gcBottleCodeEmpty)
            return ResultCode.BOTTLE_CODE_EMPTY;

        gcImportMode = type == RecordFragment.RecordType.IMPORT;

        if (gcImportMode)
            gcZeroWeight = record.importWeight == 0;
        else
            gcZeroWeight = record.exportWeight == 0;
        if (gcZeroWeight)
            return ResultCode.ZERO_WEIGHT;

        gcValidExportBottleCode = false;

        for (Record r:
             mRecords) {
            gcSameBottleCode = r.bottleCode.equals(record.bottleCode) ;
            gcExported = r.exportDate > 0;

            if (gcImportMode && gcSameBottleCode && !gcExported) {
                return ResultCode.NOT_EXPORT_YET;
            }

            gcValidExportBottleCode |= gcSameBottleCode;
        }

        if (!gcImportMode && !gcValidExportBottleCode)
            return ResultCode.NOT_IMPORT_YET;

        return ResultCode.VALID;
    }

    private RecordHelper() {

    }
}
