package com.app.lethang.thtgasreporter.Adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.app.lethang.thtgasreporter.Helper.GasHelper;
import com.app.lethang.thtgasreporter.Model.Gas;
import com.app.lethang.thtgasreporter.Model.Record;
import com.app.lethang.thtgasreporter.R;

import java.util.ArrayList;

public class GasSpinnerAdapter extends ArrayAdapter<Gas> {
    private Context mContext;
    private ArrayList<Gas> listGas;
    private int mResourceId;

    public GasSpinnerAdapter(@NonNull Context context, int resource, GasHelper helper) {
        super(context, resource);
        listGas = helper.getAllGas();
        mResourceId = resource;
        mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(mResourceId, null);
        TextView textView = view.findViewById(android.R.id.text1);
        textView.setText(listGas.get(position).name);
        if (position % 2 == 0)
            textView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        return view;
        //return createTextViewWithTextColor(position, android.R.color.black);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createTextViewWithTextColor(position, android.R.color.holo_red_dark);
    }


    private TextView createTextViewWithTextColor(int position, int colorId) {
        TextView textView = new TextView(mContext);
        textView.setText(listGas.get(position).name);
        textView.setTextColor(mContext.getResources().getColor(colorId));
        return textView;
    }

    @Override
    public int getCount() {
        return listGas.size();
    }

    public int getPosition(Record record) {
        for (Gas gas :
                listGas) {
            if (gas.id == record.gasType)
                return listGas.indexOf(gas);
        }
        return -1;
    }
}
