package com.app.lethang.thtgasreporter.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.lethang.thtgasreporter.Model.Record;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private Context _context;

    private String DB_FOLDER = "database";
    private static String DB_NAME = "GasReport.db";

    SQLiteDatabase myDataBase;

    private String getMainDBPath() {
        return _context.getDatabasePath(DB_NAME).getPath();
    }

    public DBHelper(Context context) throws IOException {
        super(context, DB_NAME, null, 1);
        this._context = context;

        boolean dbexist = checkDatabase();
        if (dbexist) {
            System.out.println("Database exists");
            openDatabase();
        } else {
            System.out.println("Database doesn't exist. Copy from origin");
            try {
                createDatabase();
            } catch (Exception e) {
                Log.e("DBHelper", e.getMessage());
            }

        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

//        sqLiteDatabase.execSQL(
//                "CREATE TABLE IF NOT EXISTS " + table_name +
//                        "(id integer primary key, user_name text, phone_number text)"
//        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + table_name);
    }


    // INTERFACE
    public Cursor getTable(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + name,
                null);
        return res;
    }

    // PRIVATE
    public boolean checkDatabase() {
        SQLiteDatabase checkdb = null;
        //boolean checkdb = false;
        try {
            String path = getMainDBPath();
            checkdb = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
            //checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
        }

        return checkdb != null;
    }

    public void createDatabase() throws IOException {
        boolean dbexist = checkDatabase();
        if (dbexist) {
            //System.out.println(" Database exists.");
        } else {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private void copyDatabase() throws IOException {

        //Open your local db as the input stream
        InputStream myInput = _context.getAssets().open(DB_NAME);

        //Create empty database
        myDataBase = _context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(myDataBase.getPath());

        // transfer byte to inputfile to outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDatabase() throws SQLiteException {
        //Open the database
        String myPath = getMainDBPath();
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public synchronized void close() {
        if (myDataBase != null) {
            myDataBase.close();
        }
        super.close();
    }

    // Data handle
    public String[] getColumnsName(String tableName) {
        Cursor dbCursor = myDataBase.query(tableName, null, null, null, null, null, null);
        return dbCursor.getColumnNames();
    }

    public String[] getTableNames() {
        ArrayList<String> arrTblNames = new ArrayList<String>();
        Cursor c = myDataBase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                arrTblNames.add(c.getString(c.getColumnIndex("name")));
                c.moveToNext();
            }
        }

        return arrTblNames.toArray(new String[arrTblNames.size()]);
    }

    // GAS HELPER
    public Cursor getGasTypes() {
        try {
            return myDataBase.query(GasHelper.TABLE_NAME, null, null, null, null, null, GasHelper.COL_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Cursor getSuppliers() {
        try {
            return myDataBase.query(SupplierHelper.TABLE_NAME, null, null, null, null, null, SupplierHelper.COL_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Cursor getRecords() {
        try {
            return myDataBase.query(RecordHelper.TABLE_NAME, null, null, null, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Cursor getRecords(long fromDay, long toDay) {
        try {
            return myDataBase.query(RecordHelper.TABLE_NAME, null, "ngay_truoc >= ? AND (ngay_sau <= ? OR ngay_sau=0)", new String[]{String.valueOf(fromDay), String.valueOf(toDay)}, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    SQLiteDatabase gcWriteableDatabase;
    ContentValues gcContentValues;

    String gcWhereClause;
    String[] gcWhereArgs;

    int gcEffectRowUpdate ;

    public boolean insertRecord(Record record) {
        try {
            gcWriteableDatabase = this.getWritableDatabase();
            gcContentValues = new ContentValues();

            gcContentValues.put(RecordHelper.COL_BOTTLE_CODE, record.bottleCode);
            gcContentValues.put(RecordHelper.COL_GASTYPE, record.gasType);
            gcContentValues.put(RecordHelper.COL_SUPPLIER, record.supplier);
            gcContentValues.put(RecordHelper.COL_IMPORT_DATE, record.importDate);
            gcContentValues.put(RecordHelper.COL_IMPORT_WEIGHT, record.importWeight);
            gcContentValues.put(RecordHelper.COL_EXPORT_DATE, record.exportDate);
            gcContentValues.put(RecordHelper.COL_EXPORT_WEIGHT, record.exportWeight);

            long index = gcWriteableDatabase.insert(RecordHelper.TABLE_NAME, null, gcContentValues);
            return index >= 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateRecord(Record record) {
        try {
            gcWriteableDatabase = this.getWritableDatabase();
            gcContentValues = new ContentValues();

            gcWhereClause = RecordHelper.COL_BOTTLE_CODE + "=?";
            gcWhereArgs = new String[] {record.bottleCode};

            gcContentValues.put(RecordHelper.COL_EXPORT_DATE, record.exportDate);
            gcContentValues.put(RecordHelper.COL_EXPORT_WEIGHT, record.exportWeight);

            gcEffectRowUpdate = gcWriteableDatabase.update(RecordHelper.TABLE_NAME, gcContentValues, gcWhereClause, gcWhereArgs);

            if (gcEffectRowUpdate > 1)
                Log.w(this.getClass().toString(), "Update effect more than 1 row -> " + gcEffectRowUpdate);

            return gcEffectRowUpdate > 0;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}
