package com.app.lethang.thtgasreporter.Helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;

public class MailHelper {
    private Context _context;

    public MailHelper(Context context) {
        _context = context;
    }

    public void sendEmailTo(String[] emails, File attachFile) {
        try {
            Uri path = Uri.fromFile(attachFile);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            // set the type to 'email'
            emailIntent .setType("vnd.android.cursor.dir/email");
            //String to[] = {"vic@artistsworld.me"};
            emailIntent .putExtra(Intent.EXTRA_EMAIL, emails);
            // the attachment
            emailIntent .putExtra(Intent.EXTRA_STREAM, path);
            // the mail subject
            emailIntent .putExtra(Intent.EXTRA_SUBJECT, "Subject");
            _context.startActivity(Intent.createChooser(emailIntent , "Send email..."));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
