package com.app.lethang.thtgasreporter.Helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.app.lethang.thtgasreporter.Config.RSLabel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermissionHelper {
    private Context _context;

    public PermissionHelper(Context context) {
        _context = context;
    }

    public void checkPermissionRequestIfNeeded() {
        List<String> permissions = new ArrayList<>(Arrays.asList(RSLabel.appPermissions));

        int pos = 0;
        while (pos < permissions.size()) {
            if (checkPermission(permissions.get(pos)))
                permissions.remove(pos);
            else
                pos++;
        }

        if (permissions.size() == 0)
            return;

        requestPermission(permissions.toArray(new String[permissions.size()]));

    }

    boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(_context,
                permission) == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermission(String[] permissions) {
        ActivityCompat.requestPermissions((Activity) _context,
                permissions,
                RSLabel.MY_APP_REQUEST_CODE);
    }
}
