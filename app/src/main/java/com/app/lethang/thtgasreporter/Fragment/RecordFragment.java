package com.app.lethang.thtgasreporter.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.lethang.thtgasreporter.Adapter.GasSpinnerAdapter;
import com.app.lethang.thtgasreporter.Adapter.SupplierSpinnerAdapter;
import com.app.lethang.thtgasreporter.Helper.DBHelper;
import com.app.lethang.thtgasreporter.Helper.GasHelper;
import com.app.lethang.thtgasreporter.Helper.RecordHelper;
import com.app.lethang.thtgasreporter.Helper.SupplierHelper;
import com.app.lethang.thtgasreporter.Model.Record;
import com.app.lethang.thtgasreporter.R;
import com.app.lethang.thtgasreporter.TabActivity;

import java.util.Calendar;
import java.util.Locale;


public class RecordFragment extends Fragment implements View.OnClickListener {
    public enum RecordType {IMPORT, EXPORT}

    private static final String ARG_RECORD_TYPE = "record_type";

    private RecordType mRecordType;

    private Spinner mSupplierSpinner, mGasTypeSpinner;
    private Button mSaveButton;
    private EditText mWeightEditText, mBottleCodeEditText;

    private View mView;
    private Context mContext;
    private DBHelper mDBHelper;

    private GasHelper mGasHelper;
    private SupplierHelper mSupplierHelper;
    private RecordHelper mRecordHelper;

    private SupplierSpinnerAdapter mSupplierAdapter;
    private GasSpinnerAdapter mGasAdapter;

    //private OnFragmentInteractionListener mListener;

    public RecordFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RecordFragment newInstance(RecordType type) {
        RecordFragment fragment = new RecordFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_RECORD_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRecordType = (RecordType) getArguments().get(ARG_RECORD_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_gas_import, container, false);
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        setUpUIComponent();
        setupHelpers();
        setupSpinner();
        toggleUIBaseOnRecordType();
        setTextChangeListenerBaseOnRecordType();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mSaveButton.getId()) {
            if (mRecordType == RecordType.IMPORT)
                saveImportRecord();
            else
                saveExportRecord();
            cleanUpUI();
        }
    }

    private void cleanUpUI() {
        mBottleCodeEditText.getText().clear();
        mWeightEditText.getText().clear();
        mBottleCodeEditText.requestFocus();
    }

    private void toggleUIBaseOnRecordType() {
        if (mRecordType == RecordType.EXPORT) {
            mSupplierSpinner.setEnabled(false);
            mSupplierSpinner.setClickable(false);

            mGasTypeSpinner.setEnabled(false);
            mGasTypeSpinner.setClickable(false);
        }
    }

    private String gcCurrentBottleCode;
    private Record gcCurrentRecord;
    private int gcPositionInSpinner;

    private void setTextChangeListenerBaseOnRecordType() {
        if (mRecordType == RecordType.EXPORT) {
            mBottleCodeEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    gcCurrentBottleCode = mBottleCodeEditText.getText().toString();
                    if (gcCurrentBottleCode.length() > 0 && !hasFocus) {
                        updateSpinnerBaseOnBottleCode();
                    }
                }
            });
        }
    }

    private void updateSpinnerBaseOnBottleCode() {
        gcCurrentRecord = mRecordHelper.getRecord(gcCurrentBottleCode);
        if (gcCurrentRecord == null)
            return;

        gcPositionInSpinner = mGasAdapter.getPosition(gcCurrentRecord);
        if (gcPositionInSpinner > -1)
            mGasTypeSpinner.setSelection(gcPositionInSpinner, true);

        gcPositionInSpinner = mSupplierAdapter.getPosition(gcCurrentRecord);
        if (gcPositionInSpinner > -1)
            mSupplierSpinner.setSelection(gcPositionInSpinner, true);
    }

    private void setUpUIComponent() {
        mBottleCodeEditText = mView.findViewById(R.id.editText_bottleCode);
        mWeightEditText = mView.findViewById(R.id.editText_weight);
        mSupplierSpinner = mView.findViewById(R.id.spinner_supplier);
        mGasTypeSpinner = mView.findViewById(R.id.spinner_gasType);
        mSaveButton = mView.findViewById(R.id.button_save);
        mSaveButton.setOnClickListener(this);
    }

    private void setupSpinner() {
        mGasAdapter = new GasSpinnerAdapter(mContext, R.layout.spinner_dropdown_item, mGasHelper);
        mGasAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mGasTypeSpinner.setAdapter(mGasAdapter);

        mSupplierAdapter = new SupplierSpinnerAdapter(mContext, R.layout.spinner_dropdown_item, mSupplierHelper);
        mSupplierSpinner.setAdapter(mSupplierAdapter);
    }


    private void setupHelpers() {
        try {
            if (mContext instanceof TabActivity) {
                mDBHelper = ((TabActivity) mContext).mDbHelper;
                if (mDBHelper != null) {
                    mGasHelper = GasHelper.getInstance(mDBHelper, true);
                    mSupplierHelper = SupplierHelper.getInstance(mDBHelper, true);
                    mRecordHelper = RecordHelper.getInstance(mDBHelper, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveImportRecord() {
        try {
            Record record = new Record();
            record.bottleCode = mBottleCodeEditText.getText().toString();
            record.gasType = (mGasHelper.getAllGas().get(mGasTypeSpinner.getSelectedItemPosition())).id;
            record.supplier = (mSupplierHelper.getSuppliers().get(mSupplierSpinner.getSelectedItemPosition())).id;
            record.importWeight = Float.valueOf(mWeightEditText.getText().toString());
            record.importDate = Calendar.getInstance(Locale.getDefault()).getTime().getTime();
            RecordHelper.ResultCode resultCode = mRecordHelper.importGas(record);
            if (resultCode == RecordHelper.ResultCode.VALID) {
                Toast.makeText(mContext, R.string.mess_import_success, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, resultCode.toString(), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveExportRecord() {
        try {
            Record record = new Record();
            record.bottleCode = mBottleCodeEditText.getText().toString();
            record.exportWeight = Float.valueOf(mWeightEditText.getText().toString());
            record.exportDate = Calendar.getInstance(Locale.getDefault()).getTime().getTime();
            RecordHelper.ResultCode resultCode = mRecordHelper.exportGas(record);
            if (resultCode == RecordHelper.ResultCode.VALID) {
                Toast.makeText(mContext, R.string.mess_export_success, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, resultCode.toString(), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
